@extends('layouts.default')

@section('content')
  @if (Auth::check())
    <div class="row">
      <div class="col-md-8">
        <h3>LIST</h3>
        @include('shared._feed')
      </div>
      <aside class="col-md-4">
        <section class="user_info">
          @include('shared._user_info', ['user' => Auth::user()])
        </section>
        <section class="stats">
          @include('shared._stats', ['user' => Auth::user()])
        </section>
        <section class="status_form">
          @include('shared._status_form')
        </section>
      </aside>
    </div>
  @else
    <div class="jumbotron">
      <h1>Hello </h1>
      <p>
        <a class="btn btn-lg btn-success" href="/example/">example</a>
        <a class="btn btn-lg btn-success" href="{{ route('signup') }}">register</a>
      </p>
    </div>
  @endif
@stop
