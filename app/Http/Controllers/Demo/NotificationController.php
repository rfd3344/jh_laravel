<?php

namespace App\Http\Controllers\Demo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;

class NotificationController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $view = 'emails.example';
        // Directory: resources\views\emails\example.blade.php
        $data = ['title'=>'This is Title'];
        $res = Mail::send($view, $data,
            function($message){
                $message
                // default from .env MAIL_FROM_ADDRESS
                // ->from('admin@mail.com', 'adminName')
                ->to('use@mail.com','userName')
                ->subject('Subject');
            }
        );
        dump('email sent');
        return view('demo.test');
    }

    public function store()
    {
        // Post method

    }
}
