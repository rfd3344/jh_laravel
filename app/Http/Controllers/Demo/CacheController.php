<?php

namespace App\Http\Controllers\Demo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use App\Models\User;



class CacheController extends Controller
{
    public function __construct()
    {

    }

    public function index(User $user)
    {
        $seconds = Carbon::now()->addMinutes(10);
        Cache::put('key1', 'val1', $seconds );
        // Cache::store('key1')->get('value1');
        $value = Cache::remember('users', $seconds, function() use($user) {
            return $user->get();
        });
        dump( $value );
        dump( Cache::get('users') );
        dump( csrf_token()  );

        return view( 'demo/test');
    }

    public function store(User $user)
    {
        // Post method

    }


}
