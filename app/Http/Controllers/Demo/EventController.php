<?php

namespace App\Http\Controllers\Demo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Events\DemoEvent;

class EventController extends Controller
{
    public function __construct()
    {

    }

    public function index(User $user)
    {
        event(new DemoEvent());
        return view( 'static_pages/email');
    }

    public function store(User $user)
    {
        // Post method

    }
}
