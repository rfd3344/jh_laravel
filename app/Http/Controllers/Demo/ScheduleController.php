<?php

namespace App\Http\Controllers\Demo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pusher\Pusher;

class ScheduleController extends Controller
{
    public function __construct()
    {
        
    }

    public function index()
    {
        return view( 'demo/schedule');
    }

    public function store(Request $request)
    {
        $pusherConfig = config('broadcasting.connections.pusher');
        $pusher = new Pusher(
            $pusherConfig['key'],
            $pusherConfig['secret'],
            $pusherConfig['app_id'],
            [ 'cluster' => $pusherConfig['options']['cluster'] ]
        );
        $data['text_input'] = $request->text_input;
        $pusher->trigger('my-channel', 'my-event', $data);

        return view( 'demo/schedule');
    }

    public function show( )
    {

    }
}
