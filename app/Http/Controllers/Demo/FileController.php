<?php

namespace App\Http\Controllers\Demo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class FileController extends Controller
{
    public function __construct()
    {
        // php artisan storage:link


    }

    public function index()
    {
        Storage::put('file.txt', 'dir: storage\app\public\file.txt');
        $contents = Storage::get('file.txt');
        dump($contents);

        return view('demo/file');
    }

    public function store(Request $request )
    {
        dump('store');
        dump( $request->all() );
        dump( $request->text_input );
        dump( $request->file_input );

        $path = $request->file_input->store('demo');
        dump(route('home') . '/storage/' . $path);


        return view('demo/file');
    }
}
