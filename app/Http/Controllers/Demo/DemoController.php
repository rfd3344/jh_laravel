<?php

namespace App\Http\Controllers\Demo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class DemoController extends Controller
{
    public function __construct()
    {
        
    }

    public function index(){
        dump('demo');
        return view('demo/test');
    }

    public function create(){
        dump('create');

    }

    public function store(){
        dump('post store');

    }

    public function show( $item ){
        dump($item);

    }

    public function edit( Request $request ){
        dump('edit');

    }

    public function update( Request $request ){
        dump('update');
    }

    public function destroy( Request $request ){
        dump('destroy');
    }


}
