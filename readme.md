# Introduction
this is the example for Laravel
including:
    - Model, Controller  
    - DB Connection
    - Jobs and Tasks
    - Email sending (mailtrap)
## How to start
  1. Install
    - composer install
    - npm install
  2. change .env
    - APP_URL: http://localhost/jh-laravel/public/
    - DB_DATABASE: jh_laravel
  3. Database
    - create database: jh_laravel
    - php artisan migrate
    - php artisan db:seed


# Info
Version: Laravel Framework 5.5-dev
## account
  admin@email.com
  123456
